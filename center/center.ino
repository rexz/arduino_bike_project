#include <elapsedMillis.h>
#include <SoftwareSerial.h>
int relay1=6;
int relay2=7;
int relay3=8;
int relay4=9;
int cardsender = 6;
char ch1,ch2;
#define EOL_DELIMITER "\n"
SoftwareSerial mySerial(10,11); // RX, TX
void setup(){
   Serial.begin(9600);
   mySerial.begin(38400);
   pinMode(relay1, OUTPUT); 
   pinMode(relay2, OUTPUT);   
   pinMode(relay3, OUTPUT); 
   pinMode(relay4, OUTPUT); 
   pinMode(cardsender, INPUT);  
   digitalWrite(cardsender, HIGH);
   pinMode(cardsender, OUTPUT);
   digitalWrite(relay1, HIGH);
   digitalWrite(relay2, HIGH);
   digitalWrite(relay3, HIGH);
   digitalWrite(relay4, HIGH);
   
}
void loop(){
    //SERIAL PORT TO MAC
    if (Serial.available() > 0) {
      char ch1 = Serial.read();
      //READ TIMEOUT COMMAND FROM MAC
      if (ch1 == 't') {
        //SEND TO TIMEOUT COMMAND TO SALVE MODULE
        mySerial.print("T");
      }  
    }
    if ( mySerial.available() > 0) {
      char ch2 = mySerial.read();
      //RECEIVE START COMMAND ORM MASTER 
      if (ch2 == 'S') {
        //SEND START COMMAND TO MAC
        Serial.print("START");
        Serial.print(EOL_DELIMITER);
          
      }
      //RECEIVE RESET COMMAND FORM MASTER
      if (ch2 == 'R') {
        //SEND FAILURE COMMAND TO MAC
        Serial.print("FAILURE");
        Serial.print(EOL_DELIMITER);
        digitalWrite(relay1, HIGH);
        digitalWrite(relay2, HIGH);
        digitalWrite(relay3, HIGH);
        digitalWrite(relay4, HIGH);
      }
      //FOR 4 LED 
      if (ch2 == 'A') {
        digitalWrite(relay1, LOW);
      } 
      if (ch2== 'B') {
        digitalWrite(relay2, LOW);
      }
      if (ch2 == 'C') {
        digitalWrite(relay3, LOW);
      } 
      if (ch2 == 'D') {
        //SEND REACH COMMAND TO MAC
        Serial.print("REACH");
        Serial.print(EOL_DELIMITER); 
        digitalWrite(relay4, LOW);
      } 
      if (ch2 == 'F') {
        //CARD OUT COMMAND
        //digitalWrite(cardsender, HIGH);
        //delay(100);
        //digitalWrite(cardsender, LOW);
      } 
  }   
}
